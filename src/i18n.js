/**
 * i18n.js
 *
 * This will setup the i18n language files and locale data for your app.
 *
 *   IMPORTANT: This file is used by the internal build
 *   script `extract-intl`, and must use CommonJS module syntax
 *   You CANNOT use import/export in this file.
 */
const addLocaleData = require('react-intl').addLocaleData; //eslint-disable-line
const enLocaleData = require('react-intl/locale-data/en');
const deLocaleData = require('react-intl/locale-data/de');

const enTranslationMessages = require('./translations/en.json');
const deTranslationMessages = require('./translations/de.json');

addLocaleData(enLocaleData);
addLocaleData(deLocaleData);

const _DEFAULT_LOCALE = 'en';

// prettier-ignore
const _appLocales = [
  'en',
  'de',
];

const _formatTranslationMessages = (locale, messages) => {
  const defaultFormattedMessages =
    locale !== _DEFAULT_LOCALE
      ? _formatTranslationMessages(_DEFAULT_LOCALE, enTranslationMessages)
      : {};
  const flattenFormattedMessages = (formattedMessages, key) => {
    const formattedMessage =
      !messages[key] && locale !== _DEFAULT_LOCALE
        ? defaultFormattedMessages[key]
        : messages[key];
    return Object.assign(formattedMessages, { [key]: formattedMessage });
  };
  return Object.keys(messages).reduce(flattenFormattedMessages, {});
};

const _translationMessages = {
  en: _formatTranslationMessages('en', enTranslationMessages),
  de: _formatTranslationMessages('de', deTranslationMessages),
};

export const DEFAULT_LOCALE = _DEFAULT_LOCALE;
export const appLocales = _appLocales;
export const translationMessages = _translationMessages;
export const formatTranslationMessages = _formatTranslationMessages;
