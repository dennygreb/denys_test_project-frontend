import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';

const LoadingIndicator = () => (
  <CircularProgress size={50} />
);

export default LoadingIndicator;
