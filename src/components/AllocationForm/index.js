import React from "react";
import { Formik, Form, Field, FieldArray } from "formik";

const AllocationForm = ({ onSubmit }) => (
  <div>
    <h1>Get allocation data based on components you provide</h1>
    <Formik
      initialValues={{ components: [{ isin: '', quantity: ''}]}}
      onSubmit={onSubmit}
      render={({ values }) => (
        <Form>
          <FieldArray
            name="components"
            render={arrayHelpers => (
              <div>
                { values.components.map((friend, index) => (
                    <div key={index}>
                      <Field name={`components.${index}.isin`} />
                      <Field name={`components.${index}.quantity`} />
                      <button
                        type="button"
                        onClick={() => arrayHelpers.push({ isin: '', quantity: '' })}
                      >
                        +
                      </button>
                    </div>
                ))}
                <div>
                  <button type="submit">Submit</button>
                </div>
              </div>
            )}
          />
        </Form>
      )}
    />
  </div>
);

export default AllocationForm;