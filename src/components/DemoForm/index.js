import React from 'react';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

const style = {
  margin: 20,
}

const UserForm = (props) => {
  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
  } = props;

  return(
    <form onSubmit={handleSubmit}>
      <FormControl error={errors.email && touched.email}>
        <InputLabel htmlFor="email">Email</InputLabel>
        <Input name="email" id="email" value={values.email} onChange={handleChange} onBlur={handleBlur}/>
        {errors.email && touched.email && <FormHelperText id="email-text">{errors.email}</FormHelperText>}
      </FormControl>
      <br />
      <FormControl error={errors.username && touched.username}>
        <InputLabel htmlFor="username">Username</InputLabel>
        <Input name="username" id="username" value={values.username} onChange={handleChange} onBlur={handleBlur}/>
        {errors.username && touched.username && <FormHelperText id="username-text">{errors.username}</FormHelperText>}
      </FormControl>
      <br />
      <Button
        type="submit"
        variant="contained"
        disabled={isSubmitting}
        style={style}
      >
        {isSubmitting ? 'WAIT...' : 'SUBMIT'}
      </Button>
    </form>
  );
}

const EnhancedForm = withFormik({
  mapPropsToValues: (props) => ({ 
    email: props.user.email,
    username: props.user.username,
  }),
  validationSchema: Yup.object().shape({
    email: Yup.string().email('Invalid email address').required('Email is required'),
    username: Yup.string().required('Username is required'),
  }),
  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  },
  displayName: 'BasicForm',
})(UserForm);

export default EnhancedForm;
