/**
 *
 * LocaleToggle
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';


function Toggle(props) {
  let content = <MenuItem>--</MenuItem>;

  // If we have items, render them
  if (props.values) {
    content = props.values.map(value => (
      <MenuItem key={value} value={value}>
        {props.intl.formatMessage(props.messages[value])}
      </MenuItem>
    ));
  }

  return (
    <Select value={props.value} onChange={props.onToggle}>
      {content}
    </Select>
  );
}

Toggle.propTypes = {
  onToggle: PropTypes.func,
  values: PropTypes.array,
  value: PropTypes.string,
  messages: PropTypes.object,
  intl: intlShape.isRequired,
};

export default injectIntl(Toggle);
