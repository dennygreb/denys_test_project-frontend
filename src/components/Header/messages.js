/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'boilerplate.components.Header.title',
    defaultMessage: 'REACT SEED',
  },
  features: {
    id: 'boilerplate.components.Header.features',
    defaultMessage: 'Features',
  },
});
