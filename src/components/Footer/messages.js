/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  footerMessage: {
    id: 'boilerplate.components.Footer.footer.message',
    defaultMessage: 'Footer message.',
  },
  authorMessage: {
    id: 'boilerplate.components.Footer.author.message',
    defaultMessage: `
      Fincite.
    `,
  },
});
