import React from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from'@material-ui/core/Typography';

import LocaleToggle from '../../containers/LocaleToggle';
import messages from './messages';
import styles from './styles';


function Footer(props) {
  const { classes } = props;
  return (
    <footer className={classes.footer}>
      <Typography>
        <FormattedMessage {...messages.footerMessage} />
      </Typography>
      <div>
        <LocaleToggle />
      </div>
      <Typography>
        <FormattedMessage
          {...messages.authorMessage}
        />
      </Typography>
    </footer>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);
