export default (theme) => ({
  footer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 100,
    background: theme.palette.grey[200],
  },
});