/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'

import messages from './messages';
import DemoForm from '../../components/DemoForm';

export default class FeaturePage extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  // Since state and props are static,
  // there's no need to re-render this component
  shouldComponentUpdate() {
    return false;
  }

  render() {

    const styleVariantH5 = {
      marginTop: 30,
      marginBottom: 30,
    };

    const randomValues = Array(100).fill().map(() => Math.random());

    return (
      <div
        style={{
          padding: 20
        }}
      >
        <Helmet>
          <title>Feature Page</title>
          <meta
            name="description"
            content="Feature page of React.js Boilerplate application"
          />
        </Helmet>
        <Typography variant="h5" style={styleVariantH5}>
          <FormattedMessage {...messages.header} />
        </Typography>

        <Paper
          style={{
            width: '100%',
            overflowX: 'auto',
          }}
        >
          <Table>
            <TableHead>
              <TableRow>
                <TableCell><FormattedMessage {...messages.featuresHeader} /></TableCell>
                <TableCell><FormattedMessage {...messages.descriptionHeader} /></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell><FormattedMessage {...messages.scaffoldingHeader} /></TableCell>
                <TableCell><FormattedMessage {...messages.scaffoldingMessage} /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell><FormattedMessage {...messages.feedbackHeader} /></TableCell>
                <TableCell><FormattedMessage {...messages.feedbackMessage} /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell><FormattedMessage {...messages.routingHeader} /></TableCell>
                <TableCell><FormattedMessage {...messages.routingMessage} /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell><FormattedMessage {...messages.networkHeader} /></TableCell>
                <TableCell><FormattedMessage {...messages.networkMessage} /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell><FormattedMessage {...messages.intlHeader} /></TableCell>
                <TableCell><FormattedMessage {...messages.intlMessage} /></TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>

        <Typography variant="h5" style={styleVariantH5}>
          <FormattedMessage {...messages.forms} />
        </Typography>

        <Typography>
          <FormattedMessage {...messages.formsMessage} />
        </Typography>
        <DemoForm
          user={{
            email: '',
            username: '',
          }}
        />

        <Typography variant="h5" style={styleVariantH5}>
          <FormattedMessage {...messages.highcharts} />
        </Typography>

        <Typography>
          <FormattedMessage {...messages.highchartsMessage} />
        </Typography>

        <HighchartsReact
          highcharts={Highcharts}
          constructorType={'stockChart'}
          options={{
            series: [{
              data: randomValues
            }]
          }}
        />
      </div>
    );
  }
}
