export default (theme) => ({
  icon: {
    margin: theme.spacing.unit * 2,
    fontSize: 350,
  },
  button: {
    margin: theme.spacing.unit * 2,
  },
});