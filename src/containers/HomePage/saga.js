
import { call, put, takeLatest } from 'redux-saga/effects';
import { 
  allocationDataLoadingError,
  allocationDataLoadingSuccess,
} from './../../containers/HomePage/actions';
import { 
  FETCH_ALLOCATION_DATA_START,
} from './../../containers/HomePage/constants';

import { postRequest } from '../../utils/request';


export function* getAllocationData(action) {
  const requestURL = '/api/v1/charts/data/';

  try {
    const data = yield call(postRequest, requestURL, action.values);
    yield put(allocationDataLoadingSuccess(data));
  } catch (err) {
    yield put(allocationDataLoadingError(err));
  }
}

export default function* allocationData() {
  yield takeLatest(FETCH_ALLOCATION_DATA_START, getAllocationData);
}
