/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.get('home', initialState);

const makeSelectAllocationData = () =>
  createSelector(selectHome, homeState => homeState.get('allocationData'));

const makeSelectUsername = () =>
  createSelector(selectHome, homeState => homeState.get('username'));

export { selectHome, makeSelectUsername, makeSelectAllocationData };
