/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { AllocationPieChart } from 'fincite-react-charts';

import Grid from '@material-ui/core/Grid';

import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';

import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from '../../containers/App/selectors';
import { loadRepos } from '../App/actions';
import { changeUsername, fetchAllocationData } from './actions';
import { makeSelectUsername, makeSelectAllocationData } from './selectors';

import reducer from './reducer';
import saga from './saga';
import styles from './styles';

import AllocationForm from '../../components/AllocationForm';


export class HomePage extends React.Component {
  getAllocation = (values) => {
    const requestValues = values.components.map(component => ({ ...component, quantity: Number(component.quantity) }));
    this.props.fetchAllocationData(requestValues);
  }

  render() {
    const { allocationData } = this.props;
    return (
      <React.Fragment>
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="A React.js Boilerplate application homepage"
          />
        </Helmet>
        <Grid container justify="center">
          <AllocationForm onSubmit={this.getAllocation}/>
        </Grid>
        <Grid container justify="center">
          { 
            allocationData && 
              <AllocationPieChart
                chartOptions={{
                  legend: {
                    align: 'center',
                    layout: 'vertical',
                    verticalAlign: 'bottom'
                  }
                }}
                group="by_sector"
                legendInteraction={true}
                title="Sector Allocation Pie Chart, minimal"
                data={allocationData}
              />
          }
        </Grid>
      </React.Fragment>
    );
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    fetchAllocationData: values => dispatch(fetchAllocationData(values)),
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  allocationData: makeSelectAllocationData(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withStyles(styles),
)(HomePage);
