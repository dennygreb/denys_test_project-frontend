/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  startProjectHeader: {
    id: 'boilerplate.containers.HomePage.start_project.header',
    defaultMessage: 'Start your next react project in seconds',
  },
  startProjectMessage: {
    id: 'boilerplate.containers.HomePage.start_project.message',
    defaultMessage:
      'A highly scalable, offline-first foundation with the best DX and a focus on performance and best practices',
  },
  loremHeader: {
    id: 'boilerplate.containers.HomePage.lorem.header',
    defaultMessage: 'Lorem ipsum',
  },
  loremMessage: {
    id: 'boilerplate.containers.HomePage.lorem.message',
    defaultMessage: `
      But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born
      and I will give you a complete account of the system, and expound the actual teachings of the great
      explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure
      itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter
      consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain
      pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can
      procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical
      exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses
      to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant
      pleasure? On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and
      demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee
    `,
  },
});
